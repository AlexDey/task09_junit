import java.util.ArrayList;
import java.util.List;

/**
 * @author Deyneka Oleksandr
 * @version 1.0 16 Dec 2018
 * lass for testing
 */
public class Application {
    public List<String> list;

    int performAddition(int limit) {
        int result = 0;
        for (int i = 0; i <= limit; i++) {
            result += i;
        }
        return result;
    }

    void initList() {
        list = new ArrayList<>();
    }

    String printSomething(String s) {
        return s + " there!";
    }
}
