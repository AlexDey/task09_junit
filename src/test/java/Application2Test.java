import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Application2Test {

    @Test
    void testPrintSomething() {
        Application application = new Application();
        String actual = application.printSomething("Hi");
        String expected = "Hi there!";
        assertEquals(expected, actual);
    }
}