import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Create some sample class. Write a test class for it that use:
 *  different asserts
 *  static final constant with package-private or protected access level
 *  mocks and Mockito.verify
 */
public class ApplicationTest {

    @Test
    void testPerformAddition() {
        Application application = new Application();
        int actual = application.performAddition(5);
        int expected = 15;
        assertEquals(expected, actual);
    }

    // test for testing void methods
    @Test
    void testInitList() {
        Application application = new Application();
        application.initList();
        assertNotNull(application.list);
    }
}